#include <algorithm>
#include <QString>
#include <KTextEditor/DocumentCursor>
#include <KTextEditor/MovingInterface>
#include "structure.h++"

void analyser(std::vector<KalinkaTitle>& ans, KTextEditor::Document* doc, const std::string& name, const std::string& symb, unsigned lvl)
{
    KTextEditor::MovingInterface* moving = qobject_cast<KTextEditor::MovingInterface*>(doc);
    KTextEditor::DocumentCursor beg(doc,KTextEditor::Cursor::start()), prev(doc);
    while((beg != doc->documentEnd()) && (beg != prev))
    {
        QVector<KTextEditor::Range> sec = doc->searchText(KTextEditor::Range(beg,doc->documentEnd()),QString::fromStdString("(^|\n|\r)([^%]|(\\\\%))*\\\\"+name+"\\*?"),KTextEditor::SearchOption::Regex);
        KTextEditor::DocumentCursor i(doc,doc->documentEnd());
        if(sec[0].isValid() && (sec[0].start() != sec[0].end()))
        {
            i = KTextEditor::DocumentCursor(doc,sec[0].end());
            if(i.move(2))
            {
                KTextEditor::Cursor begin(i);
                unsigned stack = 0;
                bool esc = false, com = false;
                do
                {
                    if(com)
                    {
                        if((doc->characterAt(i) == QChar('\n')) || (doc->characterAt(i) == QChar('\r')) || (doc->characterAt(i) == QChar('\0')))
                        {
                            com = false;
                        }
                    }
                    else if(esc)
                    {
                        esc = false;
                    }
                    else
                    {
                        if(doc->characterAt(i) == '\\')
                        {
                            esc = true;
                        }
                        else if(doc->characterAt(i) == '%')
                        {
                            com = true;
                        }
                        else if(doc->characterAt(i) == '}')
                        {
                            if(stack == 0)
                            {
                                break;
                            }
                            else
                            {
                                --stack;
                            }
                        }
                        else if(doc->characterAt(i) == '{')
                        {
                            ++stack;
                        }
                    }
                }
                while(i.move(1));
                if(i != doc->documentEnd())
                {
                    ans.push_back({symb+" "+doc->text(KTextEditor::Range(begin,i)).toStdString()+" ("+std::to_string(sec[0].end().line())+")",lvl,moving->newMovingCursor(begin)});
                }
            }
        }
        prev = beg;
        beg = i;
    }
}

std::vector<KalinkaTitle> analyseStruct(KTextEditor::Document* doc)
{
    std::vector<KalinkaTitle> ans;
    analyser(ans,doc,"part","Ⓟ",1);
    analyser(ans,doc,"chapter","Ⓒ",2);
    analyser(ans,doc,"section","Ⓢ",3);
    analyser(ans,doc,"subsection","⊙",4);
    analyser(ans,doc,"subsubsection","⚇",5);
    analyser(ans,doc,"paragraph","∘",6);
    analyser(ans,doc,"subparagraph","⋅",7);
    std::stable_sort(ans.begin(),ans.end(),[](const KalinkaTitle& x, const KalinkaTitle& y){return (x.position->line() < y.position->line());});
    return ans;
}
