#ifndef TEX_STRUCT_H
#define TEX_STRUCT_H

#include <vector>
#include <KTextEditor/Document>
#include <KTextEditor/MovingCursor>
#include <KTextEditor/MovingInterface>


struct KalinkaTitle
{
    std::string title;
    
    unsigned lvl;
    
    KTextEditor::MovingCursor* position;
};

std::vector<KalinkaTitle> analyseStruct(KTextEditor::Document* doc);
#endif
