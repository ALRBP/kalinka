#include <QtCore/QString>
#include "params.h++"

KalinkaDefaults getDefaults(const std::string& str)
{
    KalinkaDefaults ans;
    toml::table tbl = toml::parse(str);
    ans.name = tbl["plugin"]["name"].value<std::string>().value();
    ans.icon = QIcon::fromTheme(QString::fromStdString(tbl["plugin"]["icon"].value<std::string>().value()));
    ans.config = tbl["plugin"]["conf"].value<std::string>().value();
    ans.id = tbl["plugin"]["id"].value<std::string>().value();
    ans.pdfIcon = QIcon::fromTheme(QString::fromStdString(tbl["plugin"]["pdf_icon"].value<std::string>().value()));
    toml::array enb = *(tbl["formats"]["enable"].as_array());
    for(const auto& e: enb)
    {
        ans.enable.insert(e.value<std::string>().value());
    }
    toml::array cmp = *(tbl["formats"]["compile"].as_array());
    for(const auto& e: cmp)
    {
        ans.compile.insert(e.value<std::string>().value());
    }
    toml::array comp = *(tbl["compiler"].as_array());
    for(const auto& cn: comp)
    {
        toml::table c = *(cn.as_table());
        ans.compilers.push_back({c["name"].value<std::string>().value(),QIcon::fromTheme(QString::fromStdString(c["icon"].value<std::string>().value())),c["cmd"].value<std::string>().value()});
    }
    ans.errorColor = QColor(QString::fromStdString(tbl["error"]["color"].value<std::string>().value()));
    toml::array ea = *(tbl["error"]["strings"].as_array());
    for(const auto& s: ea)
    {
        ans.errors.insert(s.value<std::string>().value());
    }
    ans.warningColor = QColor(QString::fromStdString(tbl["warning"]["color"].value<std::string>().value()));
    toml::array wa = *(tbl["warning"]["strings"].as_array());
    for(const auto& s: wa)
    {
        ans.warnings.insert(s.value<std::string>().value());
    }
    ans.noticeColor = QColor(QString::fromStdString(tbl["notice"]["color"].value<std::string>().value()));
    toml::array na = *(tbl["notice"]["strings"].as_array());
    for(const auto& s: na)
    {
        ans.notices.insert(s.value<std::string>().value());
    }
    ans.successColor = QColor(QString::fromStdString(tbl["success"]["color"].value<std::string>().value()));
    return ans;
}

std::list<KalinkaSymbolClass> getSymbols(const std::string& str)
{
    try
    {
        return YAML::Load(str).as<std::list<KalinkaSymbolClass>>();
    }
    catch (...)
    {
        return std::list<KalinkaSymbolClass>();
    }
}

void getSymbols(std::list<KalinkaSymbolClass>& ans, const std::string& str)
{
    std::list<KalinkaSymbolClass> l;
    try
    {
        l = YAML::Load(str).as<std::list<KalinkaSymbolClass>>();
    }
    catch (...){}
    for(const auto& c: l)
    {
        bool found = false;
        for(auto& d: ans)
        {
            if(c.name == d.name)
            {
                found = true;
                for(const auto& e: c.symbols)
                {
                    bool found2 = false;
                    for(const auto& f: d.symbols)
                    {
                        if(e.cmd == f.cmd)
                        {
                            found2 = true;
                        }
                    }
                    if(!found2)
                    {
                        d.symbols.push_back(e);
                    }
                }
            }
        }
        if(!found)
        {
            ans.push_back(c);
        }
    }
}
