#ifndef MULTIACTION_H
#define MULTIACTION_H

#include <QtCore/QSet>
#include <QToolButton>
#include <KActionMenu>

class MultiAction: public KActionMenu
{
    Q_OBJECT

public:
    explicit MultiAction(QObject* parent, const QString& text = QString(), const QIcon& icon = QIcon(), bool autoUpdate = true, bool dropButton = true);

    QWidget* createWidget(QWidget* parent) override;
    
    QAction* getQuickAction(void) const;
    
public slots:
    void slotSetQuickAction(QAction* action);
    
    void slotRefreshButtons(void);
    
private:
    QSet<QToolButton*> m_buttons;
    
    QAction* m_quickAction;
    
    bool updateAuto;
    
    bool menuButton;
};
#endif
