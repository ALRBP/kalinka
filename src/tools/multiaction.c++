#include <QtWidgets/QMenu>
#include "multiaction.h++"


MultiAction::MultiAction(QObject* parent, const QString& text, const QIcon& icon, bool autoUpdate, bool dropButton): KActionMenu(icon,text,parent)
{
    updateAuto = autoUpdate;
    menuButton = dropButton;
    if(dropButton)
    {
        setPopupMode(QToolButton::MenuButtonPopup);
    }
    else
    {
        setPopupMode(QToolButton::DelayedPopup);
    }
    if(updateAuto)
    {
        connect(menu(),&QMenu::triggered,this,&MultiAction::slotSetQuickAction);
    }
    connect(this,&QAction::changed,this,&MultiAction::slotRefreshButtons);
}

QWidget* MultiAction::createWidget(QWidget* parent)
{
    QToolButton* button = qobject_cast<QToolButton *>(KActionMenu::createWidget(parent));
    button->removeAction(this);
    button->setMenu(menu());
    m_buttons.insert(button);
    slotRefreshButtons();
    return button;
}

QAction* MultiAction::getQuickAction(void) const
{
    return m_quickAction;
}

void MultiAction::slotSetQuickAction(QAction* a)
{
    m_quickAction = a;
    slotRefreshButtons();
}

void MultiAction::slotRefreshButtons(void)
{
    for(const auto& b: m_buttons)
    {
        b->setDefaultAction(getQuickAction());
        b->setEnabled(isEnabled());
        if(menuButton)
        {
            b->setPopupMode(QToolButton::MenuButtonPopup);
        }
        else
        {
            b->setPopupMode(QToolButton::DelayedPopup);
        }
    }
}
