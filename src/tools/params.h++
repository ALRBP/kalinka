#ifndef KALINKA_PARAMS_H
#define KALINKA_PARAMS_H
#include <string>
#include <list>
#include <unordered_set>
#include <QtGui/QColor>
#include <QtGui/QIcon>
#include <yaml-cpp/yaml.h>
#include <toml++/toml.hpp>


struct KalinkaCompiler
{
    std::string name;
    
    QIcon icon;
    
    std::string cmd;
};

struct KalinkaDefaults
{
    std::string name;
    
    QIcon icon;
    
    std::string config;
    
    std::string id;
    
    std::unordered_set<std::string> enable;
    
    QIcon pdfIcon;
    
    std::unordered_set<std::string> compile;
    
    std::list<KalinkaCompiler> compilers;
    
    QColor errorColor;
    
    std::unordered_set<std::string> errors;
    
    QColor warningColor;
    
    std::unordered_set<std::string> warnings;
    
    QColor noticeColor;
    
    std::unordered_set<std::string> notices;
    
    QColor successColor;
};

struct KalinkaSymbol
{
    std::string sym;
    
    std::string cmd;
};

struct KalinkaSymbolClass
{
    std::string name;
    
    std::list<KalinkaSymbol> symbols;
};


namespace YAML
{
template<>
struct convert<KalinkaSymbol>
{
    static Node encode(const KalinkaSymbol& s)
    {
        Node node;
        node["sym"] = s.sym;
        node["cmd"] = s.cmd;
        return node;
    }

    static bool decode(const Node& node, KalinkaSymbol& s)
    {
        s.sym = node["sym"].as<std::string>();
        s.cmd = node["cmd"].as<std::string>();
        return true;
    }
};

template<>
struct convert<KalinkaSymbolClass>
{
    static Node encode(const KalinkaSymbolClass& c)
    {
        Node node;
        node["class"] = c.name;
        node["symbols"] = c.symbols;
        return node;
    }

    static bool decode(const Node& node, KalinkaSymbolClass& c)
    {
        c.name = node["class"].as<std::string>();
        c.symbols = node["symbols"].as<std::list<KalinkaSymbol>>();
        return true;
    }
};
}
    
    
KalinkaDefaults getDefaults(const std::string& str);

std::list<KalinkaSymbolClass> getSymbols(const std::string& str);

void getSymbols(std::list<KalinkaSymbolClass>& ans, const std::string& str);
#endif
