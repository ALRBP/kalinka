#ifndef KALINKA_VIEW_H
#define KALINKA_VIEW_H
#include <QSet>
#include <QtCore/QUrl>
#include <QtCore/QProcess>
#include <QAction>
#include <QWidget>
#include <QListWidget>
#include <QTreeWidget>
#include <QToolBox>
#include <KActionMenu>
#include <KTextEditor/MovingCursor>
#include <KParts/ReadOnlyPart>
#include <kxmlguiclient.h>
#include "tools/multiaction.h++"
#include "kalinka.h++"


class KalinkaView: public QObject, public KXMLGUIClient
{
Q_OBJECT

public:
    explicit KalinkaView(Kalinka* plugin, KTextEditor::MainWindow* mw);

    ~KalinkaView(void);

public slots:
    void slotDocChanged(void);

    void slotCompile(QAction* a);

    void slotCompiled(QProcess* p);

    void slotView(void);
    
    void slotSymb(QListWidgetItem* s);
    
    void slotUpdateStruct(void);
    
    void slotMove(QTreeWidgetItem* t);
    
signals:
    void signalCompiled(void);

private:
    KTextEditor::MainWindow* m_mainWindow;
    
    Kalinka* m_plugin;
    
    MultiAction* m_compile;
    
    QAction* m_view;
    
    QWidget* m_preview;
    
    KParts::ReadOnlyPart* m_okular;
    
    QWidget* m_out;
    
    QListWidget* m_out_text;
    
    QWidget* m_structure;
    
    QTreeWidget* m_struct;
    
    QWidget* m_symbols;
    
    QToolBox* m_symb;
    
    std::vector<KTextEditor::MovingCursor*> tree;
    
    QUrl file;
    
    bool compiling;
    
    QSet<QMetaObject::Connection*> docCon;
};
#endif
