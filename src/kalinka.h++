#ifndef KALINKA_H
#define KALINKA_H
#include <string>
#include <QtCore/QObject>
#include <QtCore/QVariant>
#include <QtCore/QList>
#include <QtCore/QSet>
#include <QWidget>
#include <KTextEditor/ConfigPage>
#include <KTextEditor/MainWindow>
#include <KTextEditor/Plugin>
#include "tools/params.h++"
#include "config/definitions.h++"

#define CONFIG "config.toml"
#define SYMB "symbols.yaml"


class KalinkaView;


class Kalinka: public KTextEditor::Plugin
{     
Q_OBJECT
    
    friend class KalinkaView;

public:
    explicit Kalinka(QObject* parent = nullptr, const QList<QVariant>& = QList<QVariant>());
    
    ~Kalinka(void) override;
    
    QObject* createView(KTextEditor::MainWindow* mw) override;
    
    int configPages(void) const override;
    
    KTextEditor::ConfigPage* configPage(int number, QWidget* parent) override;

private:
    QSet<KalinkaView*> m_views;
    
    KalinkaDefaults defaults;
    
    std::string quickCompile;
    
    std::list<KalinkaSymbolClass> sym;
};
#endif
