#include <stack>
#include <QFont>
#include <QBrush>
#include <QtWidgets/QMenu>
#include <QFileInfo>
#include <KPluginFactory>
#include <KPluginMetaData>
#include <KTextEditor/Document>
#include <KTextEditor/View>
#include <KLocalizedString>
#include <KActionCollection>
#include <KXMLGUIFactory>
#include "tools/structure.h++"
#include "kalinkaview.h++"


KalinkaView::KalinkaView(Kalinka* plugin, KTextEditor::MainWindow* mw)
{
    m_mainWindow = mw;
    m_plugin = plugin;
    KXMLGUIClient::setComponentName(QString::fromStdString(m_plugin->defaults.id),i18n(m_plugin->defaults.name.c_str()));
    setXMLFile(QStringLiteral("ui.rc"));
    connect(m_mainWindow,&KTextEditor::MainWindow::viewChanged,this,&KalinkaView::slotDocChanged);
    file = QUrl();
    m_compile = new MultiAction(this);
    actionCollection()->addAction(QStringLiteral("compile"),m_compile);
    for(const auto& c: m_plugin->defaults.compilers)
    {
        QAction* a = new QAction(c.icon,i18n(c.name.c_str()),this);
        m_compile->addAction(a);
        if(c.name == m_plugin->quickCompile)
        {
            m_compile->slotSetQuickAction(a);
        }
    }
    m_view = new QAction(m_plugin->defaults.pdfIcon,i18n("View"),this);
    actionCollection()->addAction(QStringLiteral("view"),m_view);
    m_preview = m_mainWindow->createToolView(m_plugin,QStringLiteral("kalinkapreview"),KTextEditor::MainWindow::Right,QIcon::fromTheme(QStringLiteral("kalinka")),i18n("Preview"));//TODO ask KTE devs for remove instruction
    m_out = m_mainWindow->createToolView(m_plugin,QStringLiteral("kalinkaout"),KTextEditor::MainWindow::Bottom,QIcon::fromTheme(QStringLiteral("kalinka")),i18n("LaTeX Output"));
    m_structure = m_mainWindow->createToolView(m_plugin,QStringLiteral("kalinkastructure"),KTextEditor::MainWindow::Left,QIcon::fromTheme(QStringLiteral("kalinka")),i18n("LaTeX Structure"));
    m_symbols = m_mainWindow->createToolView(m_plugin,QStringLiteral("kalinkasymbols"),KTextEditor::MainWindow::Left,QIcon::fromTheme(QStringLiteral("kalinka")),i18n("LaTeX Symbols"));
    m_out_text = new QListWidget(m_out);
    m_compile->setDisabled(true);
    m_view->setDisabled(true);
    connect(m_compile->menu(),&QMenu::triggered,this,&KalinkaView::slotCompile);
    connect(m_view,&QAction::triggered,this,&KalinkaView::slotView);
    compiling = false;
    KPluginFactory* factory = KPluginFactory::loadFactory(KPluginMetaData("okularpart")).plugin;
    QVariantList argList;
    argList << "ViewerWidget" << "ConfigFileName=kalinka-okularpartrc";
    m_okular = factory->create<KParts::ReadOnlyPart>(this,argList);
    m_okular->widget()->setParent(m_preview);
    m_symb = new QToolBox(m_symbols);
    for(const auto& l: m_plugin->sym)
    {
        QListWidget* w = new QListWidget(m_symb);
        w->setViewMode(QListView::IconMode);
        w->setMovement(QListView::Static);
        w->setResizeMode(QListView::Adjust);
        m_symb->addItem(w,QString::fromStdString(l.name));
        for(const auto& c: l.symbols)
        {
            QListWidgetItem* item = new QListWidgetItem(QString::fromStdString(c.sym));
            item->setData(Qt::ToolTipRole,QString::fromStdString(c.cmd));
            w->addItem(item);
        }
        connect(w,&QListWidget::itemActivated,this,&KalinkaView::slotSymb);
    }
    m_struct = new QTreeWidget(m_structure);
    m_struct->setHeaderLabel(i18n("Document structure"));
    connect(m_struct,&QTreeWidget::itemActivated,this,&KalinkaView::slotMove);
    slotDocChanged();
    m_plugin->m_views.insert(this);
    m_mainWindow->guiFactory()->addClient(this);
}

KalinkaView::~KalinkaView(void)
{
    for(const auto& c: docCon)
    {
        disconnect(*c);
    }
    m_mainWindow->guiFactory()->removeClient(this);
    m_plugin->m_views.remove(this);
}

void KalinkaView::slotDocChanged(void)
{
    for(const auto& c: docCon)
    {
        disconnect(*c);
    }
    if((m_mainWindow->activeView() != nullptr) && (m_mainWindow->activeView()->document() != nullptr))
    {
        docCon+= (new QMetaObject::Connection(connect(m_mainWindow->activeView()->document(),&KTextEditor::Document::documentUrlChanged,this,&KalinkaView::slotDocChanged)));
        if(m_plugin->defaults.enable.find(m_mainWindow->activeView()->document()->mimeType().toStdString()) != m_plugin->defaults.enable.end())
        {
            file = m_mainWindow->activeView()->document()->url();
            if(m_plugin->defaults.compile.find(m_mainWindow->activeView()->document()->mimeType().toStdString()) != m_plugin->defaults.compile.end())
            {
                m_compile->setEnabled(true);
                m_view->setEnabled(true);
                slotUpdateStruct();
                docCon+= (new QMetaObject::Connection(connect(m_mainWindow->activeView()->document(),&KTextEditor::Document::documentSavedOrUploaded,this,&KalinkaView::slotUpdateStruct)));
            }
            else
            {
                file = QUrl();
                m_compile->setDisabled(true);
                m_view->setDisabled(true);
                m_struct->clear();
            }
        }
        else
        {
            file = QUrl();
            m_compile->setDisabled(true);
            m_view->setDisabled(true);
            m_struct->clear();
        }
    }
    else
    {
        file = QUrl();
        m_compile->setDisabled(true);
        m_view->setDisabled(true);
        m_struct->clear();
    }
}

void KalinkaView::slotCompile(QAction* a)
{
    m_plugin->quickCompile = a->text().toStdString();
    if((!compiling) && (m_mainWindow->activeView() != nullptr) && (m_mainWindow->activeView()->document() != nullptr) && (file != QUrl()))
    {
        compiling = true;
        for(const auto& c: m_plugin->defaults.compilers)
        {
            if(a->text() == i18n(c.name.c_str()))
            {
                m_mainWindow->activeView()->document()->documentSave();
                QProcess* p = new QProcess(this);
                connect(p,QOverload<int,QProcess::ExitStatus>::of(&QProcess::finished),[this,proc=p](){slotCompiled(proc);});
                p->setWorkingDirectory(QFileInfo(file.toLocalFile()).absolutePath());
                QStringList com = QString::fromStdString(c.cmd).split(" ",Qt::SkipEmptyParts);
                QString prg = com.takeFirst();
                p->start(prg,com+QStringList(file.fileName()));
                m_out_text->clear();
                if(p->waitForStarted())
                {
                    QListWidgetItem* item = new QListWidgetItem(i18n("Compiling with ")+i18n(c.name.c_str())+i18n(" for ")+file.fileName());
                    QFont f = QFont();
                    f.setBold(true);
                    item->setFont(f);
                    m_out_text->addItem(item);
                }
                else
                {
                    QListWidgetItem* item = new QListWidgetItem(i18n("Compiling with ")+i18n(c.name.c_str())+i18n(" for ")+file.fileName()+i18n(" failed to start"));
                    QFont f = QFont();
                    f.setBold(true);
                    item->setFont(f);
                    item->setForeground(QBrush(m_plugin->defaults.errorColor));
                    m_out_text->addItem(item);
                }
                break;
            }
        }
    }
}

void KalinkaView::slotCompiled(QProcess* p)
{
    QStringList output = QString(p->readAllStandardOutput()).split('\n');
    for(const auto& l: output)
    {
        QListWidgetItem* item = new QListWidgetItem(l);
        for(const auto& s: m_plugin->defaults.errors)
        {
            if(l.contains(QString::fromStdString(s),Qt::CaseInsensitive))
            {
                item->setForeground(QBrush(m_plugin->defaults.errorColor));
                goto compiledColoringEnd;
            }
        }
        for(const auto& s: m_plugin->defaults.warnings)
        {
            if(l.contains(QString::fromStdString(s),Qt::CaseInsensitive))
            {
                item->setForeground(QBrush(m_plugin->defaults.warningColor));
                goto compiledColoringEnd;
            }
        }
        for(const auto& s: m_plugin->defaults.notices)
        {
            if(l.contains(QString::fromStdString(s),Qt::CaseInsensitive))
            {
                item->setForeground(QBrush(m_plugin->defaults.noticeColor));
                goto compiledColoringEnd;
            }
        }
        compiledColoringEnd:
        m_out_text->addItem(item);
    }
    if((p->exitStatus() == QProcess::CrashExit) || (p->exitCode() != EXIT_SUCCESS))
    {
        QListWidgetItem* item = new QListWidgetItem(i18n("Compilation failed with output: ")+((p->exitStatus() == QProcess::CrashExit)?QString::number(EXIT_FAILURE):QString::number(p->exitCode())));
        QFont f = QFont();
        f.setBold(true);
        item->setFont(f);
        item->setForeground(QBrush(m_plugin->defaults.errorColor));
        m_out_text->addItem(item);
    }
    else
    {
        QListWidgetItem* item = new QListWidgetItem(i18n("Compilation success"));
        QFont f = QFont();
        f.setBold(true);
        item->setFont(f);
        item->setForeground(QBrush(m_plugin->defaults.successColor));
        m_out_text->addItem(item);
    }
    m_out_text->scrollToBottom();
    compiling = false;
    emit signalCompiled();
}

void KalinkaView::slotView(void)
{
    QString base = file.toLocalFile();
    base.chop(4);
    m_okular->openUrl(QUrl(QString("file:")+base.append(".pdf")));
}

void KalinkaView::slotSymb(QListWidgetItem* s)
{
    if((m_mainWindow->activeView() != nullptr) && (m_mainWindow->activeView()->document() != nullptr))
    {
        m_mainWindow->activeView()->document()->insertText(m_mainWindow->activeView()->cursorPosition(),s->data(Qt::ToolTipRole).toString());
    }
}

void KalinkaView::slotUpdateStruct(void)
{
    if((m_mainWindow->activeView() != nullptr) && (m_mainWindow->activeView()->document() != nullptr))
    {
        m_struct->clear();
        std::vector<KalinkaTitle> structure = analyseStruct(m_mainWindow->activeView()->document());
        tree.clear();
        tree.reserve(structure.size());
        std::stack<QTreeWidgetItem*> itemStack;
        std::stack<unsigned> lvlStack;
        size_t i = 0;
        for(const auto& t: structure)
        {
            tree[i] = t.position;
            while((lvlStack.size() > 0) && lvlStack.top() >= t.lvl)
            {
                itemStack.pop();
                lvlStack.pop();
            }
            if(itemStack.size() == 0)
            {
                QTreeWidgetItem* item = new QTreeWidgetItem(m_struct);
                item->setText(0,QString::fromStdString(t.title));
                item->setData(0,Qt::UserRole,QVariant((uint)i++));
                m_struct->addTopLevelItem(item);
                itemStack.push(item);
                lvlStack.push(t.lvl);
            }
            else
            {
                QTreeWidgetItem* item = new QTreeWidgetItem(itemStack.top());
                item->setText(0,QString::fromStdString(t.title));
                item->setData(0,Qt::UserRole,QVariant((uint)i++));
                itemStack.top()->addChild(item);
                itemStack.push(item);
                lvlStack.push(t.lvl);
            }
        }
        m_struct->expandAll();
    }
}

void KalinkaView::slotMove(QTreeWidgetItem* t)
{
    if((m_mainWindow->activeView() != nullptr) && (m_mainWindow->activeView()->document() != nullptr))
    {
        m_mainWindow->activeView()->setCursorPosition(*(tree[t->data(0,Qt::UserRole).toInt()]));
    }
}
