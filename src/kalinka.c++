#include <fstream>
#include <filesystem>
#include <KLocalizedString>
#include <KPluginFactory>
#include <KPluginLoader>
#include <KConfig>
#include <KSharedConfig>
#include <KConfigGroup>
#include "kalinkaview.h++"
#include "kalinka.h++"

#define CONFIG_PAGES 0


K_PLUGIN_FACTORY_WITH_JSON(KalinkaFactory,"kalinka.json",registerPlugin<Kalinka>();)


Kalinka::Kalinka(QObject* parent,[[maybe_unused]] const QVariantList &args): KTextEditor::Plugin(parent)
{
    m_views = QSet<KalinkaView*>();
    std::ifstream f(std::filesystem::path(CONFIG_PATH)/CONFIG,std::ifstream::in);
    std::string s;
    std::getline(f,s,'\0');
    f.close();
    defaults = getDefaults(s);
    KConfigGroup config(KSharedConfig::openConfig(),QString::fromStdString(defaults.config));
    quickCompile = config.readEntry("quick_compile","").toStdString();
    bool resetCompile = true;
    for(const auto& c: defaults.compilers)
    {
        if(c.name == quickCompile)
        {
            resetCompile = false;
            break;
        }
    }
    if(resetCompile)
    {
        quickCompile = defaults.compilers.begin()->name;
        config.writeEntry("quick_compile",QString::fromStdString(quickCompile));
    }
    f.open(std::filesystem::path(CONFIG_PATH)/SYMB,std::ifstream::in);
    std::getline(f,s,'\0');
    f.close();
    sym = getSymbols(s);
    f.open(std::filesystem::path(QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation).toStdString())/defaults.id/SYMB,std::ifstream::in);
    if(f.is_open())
    {
        std::getline(f,s,'\0');
        f.close();
        getSymbols(sym,s);
    }
}


Kalinka::~Kalinka(void)
{
    KConfigGroup config(KSharedConfig::openConfig(),QString::fromStdString(defaults.config));
    config.writeEntry("quick_compile",QString::fromStdString(quickCompile));
}
    
QObject* Kalinka::createView(KTextEditor::MainWindow* mw)
{
    return new KalinkaView(this,mw);
}
    
int Kalinka::configPages(void) const
{
    return CONFIG_PAGES;
}

KTextEditor::ConfigPage* Kalinka::configPage([[maybe_unused]] int number, [[maybe_unused]] QWidget* parent)
{
    return nullptr;
}


#include "kalinka.moc"
